from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import portfolio_form
from lab_1.views import profile

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^$', portfolio_form, name='portfolio_form'),
    re_path(r'^profile/', profile, name='profile'),
]
