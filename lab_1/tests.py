from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
import unittest
from .views import portfolio_form, profile
from .models import Portfolio

# Create your tests here.
class Story6Unittest(TestCase):
    def test_story_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_story_landing_page_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'portfolio_form.html')

    def test_story_landing_page_using_portfolio_form_func(self):
        found = resolve('/')
        self.assertEqual(found.func, portfolio_form)

    def test_story_landing_page_has_greeting_text(self):
        request = HttpRequest()
        response = portfolio_form(request)
        html_response = response.content.decode('utf8')

        self.assertIn("Hello apa kabar?", html_response)

    def test_form_post_success_and_save_to_model(self):
        response_post = Client().post('/', {'status': 'PPW'})
        self.assertEqual(response_post.status_code, 200)

        status_count = Portfolio.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_form_show_result(self):
        Client().post('/', {'status': 'PPW'})
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('PPW', html_response)

    def test_model_can_create_new_status(self):
        new_status = Portfolio.objects.create(status='PPW')
        status_count = Portfolio.objects.all().count()
        self.assertEqual(status_count, 1)

class Challenge(TestCase):
    def test_landing_page_to_profile(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_portfolio_form_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_story_landing_page_using_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_has_name(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')

        self.assertIn("Alvin Hariman", html_response)

    def test_profile_has_npm(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')

        self.assertIn("1806205432", html_response)

    def test_profile_has_image(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')

        self.assertIn("<img ", html_response)