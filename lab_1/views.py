from django.shortcuts import render
from .forms import PortfolioForm
from .models import Portfolio

def portfolio_form(request):
    form = PortfolioForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        form = PortfolioForm()
    form_output = Portfolio.objects.all()
    context = {
        'portfolio_form' : form,
        'portfolio_form_output': form_output,
    }
    return render(request, "portfolio_form.html", context)

def profile(request):
    return render(request, 'profile.html')