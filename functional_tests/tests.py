from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options

from django.urls import reverse
import time

class FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    # Story

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(3)
        search_box = self.browser.find_element_by_name('status')
        search_box.send_keys('Coba Coba')
        time.sleep(3)
        submit = self.browser.find_element_by_xpath('//*[@id="title"]/form/button')
        submit.click()
        time.sleep(3)
        self.assertIn("Coba Coba", self.browser.page_source)

    def test_hello_apa_kabar(self):
        self.browser.get('http://127.0.0.1:8000/')
        text = self.browser.find_element_by_xpath('//*[@id="title"]/h1').text
        self.assertEquals(text, 'Hello apa kabar?')

    # Challenge

    # CSS Tests

    def test_warna_hello_apa_kabar(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertEquals(self.browser.find_element_by_xpath('//*[@id="title"]/h1').value_of_css_property("color"), "rgba(33, 37, 41, 1)")

    def test_ukuran_hello_apa_kabar(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertEquals(self.browser.find_element_by_xpath('//*[@id="title"]/h1').value_of_css_property("font-size"), "40px")

    #Layout Tests

    def test_tag_button(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertTrue(self.browser.find_element_by_tag_name('button').is_displayed)

    def test_apakah_ada_link(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertTrue(self.browser.find_element_by_tag_name('a').is_displayed)


